var Web3 = require('web3');
var web3 = new Web3();
var moment = require('moment');

/**
 * Handle arguments
 */
var minimist = require('minimist');
var args = minimist(process.argv.slice(2), {
    string: ['provider', 'startBlock', 'endBlock'],

    unknown: function () {
        console.log('Invalid arguments');
        process.exit();
    }
});


web3.setProvider(new web3.providers.HttpProvider(args.provider));

var startBlock;
var endBlock;

var blockTime = 0;
var blockTimePrev = 0;

web3.eth.getBlock('latest', function(e, res) {
    if (!args.startBlock) {
        startBlock = 0;
    } else {
        startBlock = args.startBlock;
    }

    if (!args.endBlock) {
        endBlock = res.number;
    } else {
        endBlock = args.endBlock;
    }

    console.log("Measuring TPS of the block range: " + startBlock + " - " + endBlock);

    // Initialize block times
    web3.eth.getBlock(startBlock, function(err, result) {
        blockTime = result.timestamp.toString().substr(0, result.timestamp.toString().length - 9) * 1000;
        blockTimePrev = blockTime;

        getBlockData(startBlock, endBlock);
    });
});


var batch = 0;
var batchSize = 20;
var txcount = 0;
var maxTxs = 0;
var maxTxsBlock = 0;

function getBlockData(num, endBlock) {
    if (num < endBlock) {
        web3.eth.getBlock(num, function(err, result) {
            blockTime = result.timestamp.toString().substr(0, result.timestamp.toString().length - 9) * 1000;

            // Check if current block time differs from the last one. If so, we are done with this "second"
            if (moment.utc(blockTime).diff(moment.utc(blockTimePrev)) !== 0) {
                console.log("Block (head) " + (num-1) + " TX count: " + txcount);

                txcount = 0;
                batch = 0;
            }

            num++;
            batch++;
            txcount = txcount + result.transactions.length;

            if (txcount > maxTxs) {
                maxTxs = txcount;
                maxTxsBlock = num;
            }

            blockTimePrev = blockTime;

            if (num == endBlock) {
                console.log("MAX TX count per second: " + maxTxs + " | Recorded last at: " + maxTxsBlock);
            } else {
                getBlockData(num, endBlock)
            }
        });
    }
}
